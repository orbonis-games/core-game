import { Application, Container, Graphics, Rectangle, Text } from "pixi.js";
import { Renderable } from "../renderable";
import { LoadFont } from "../../utils/load-font";
import { FontStyling } from "../../utils/font-styling";
import { CollidableShape } from "../collision";

export class GameOver extends Renderable {
    private container?: Container;
    private header?: Text;
    private subheader?: Text;
    private score?: Text;

    constructor(id: string, app: Application, protected font: FontStyling) {
        super(id, app);
    }

    public init(): Promise<void> {
        return new Promise((resolve) => {
            this.container = new Container();
            this.container.visible = false;
            this.app.stage.addChild(this.container);

            LoadFont(this.font.family).then(() => {
                this.setupElements();
                resolve();
            });
        });
    }

    public show(title: string, action: string, callback: () => void, score?: number): void {
        if (this.container && this.header && this.subheader && this.score) {
            this.header.text = title;
            this.subheader.text = `Press Space to ${action}...`;
            this.container.visible = true;
            this.score.visible = score !== undefined;
            this.score.text = `Score\n${score}`;

            const handleInput = (e: KeyboardEvent) => {
                if (e.key === " ") {
                    window.removeEventListener("keyup", handleInput);

                    this.container!.visible = false;
                    callback();
                }
            };

            window.addEventListener("keyup", handleInput);
        }
    }

    public render(delta: number): void {
        // Do nothing.
    }

    public getColliders(): CollidableShape[] {
        return [];
    }

    private setupElements(): void {
        const overlay: Graphics = new Graphics();
        overlay.beginFill(0x000000, 0.7);
        overlay.drawRect(0, 0, this.app.view.width, this.app.view.height);
        overlay.endFill();

        this.header = new Text("Game Over");
        this.header.style.fontFamily = this.font.family;
        this.header.style.fontSize = 100;
        this.header.style.fill = this.font.color;
        this.header.anchor.set(0.5, 1);
        this.header.x = this.app.view.width / 2;
        this.header.y = this.app.view.height / 2;
        
        this.subheader = new Text("Press Space to continue...");
        this.subheader.style.fontFamily = this.font.family;
        this.subheader.style.fontSize = 30;
        this.subheader.style.fill = this.font.color;
        this.subheader.style.padding = 5;
        this.subheader.anchor.set(0.5, 0);
        this.subheader.x = this.app.view.width / 2;
        this.subheader.y = this.app.view.height / 2;
        
        this.score = new Text(`Score\n0`);
        this.score.style.align = "center";
        this.score.style.fontFamily = this.font.family;
        this.score.style.fontSize = 35;
        this.score.style.fill = this.font.color;
        this.score.anchor.set(0.5, 0);
        this.score.x = this.app.view.width / 2;
        this.score.y = (this.app.view.height / 2) + 80;
        this.score.visible = false;
    
        this.container!.addChild(overlay);
        this.container!.addChild(this.header);
        this.container!.addChild(this.subheader);
        this.container!.addChild(this.score);
    }
}