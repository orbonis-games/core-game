import { Application, Container, Text } from "pixi.js";
import { Renderable } from "../renderable";
import { LoadFont } from "../../utils/load-font";
import { FontStyling } from "../../utils/font-styling";
import { CollidableShape } from "../collision";

export class ScoreBoard extends Renderable {
    private container?: Container;
    private label?: Text;

    private score: number = 0;
    private dirty: boolean = false;

    constructor(id: string, app: Application, protected font: FontStyling) {
        super(id, app);
    }

    public init(): Promise<void> {
        return new Promise((resolve) => {
            this.container = new Container();
            this.container.visible = false;
            this.app.stage.addChild(this.container);

            LoadFont(this.font.family).then(() => {
                this.setupElements();
                resolve();
            });
        });
    }

    public increaseScore(value: number): void {
        this.score += value;
        this.dirty = true;
    }

    public resetScore(): void {
        this.score = 0;
        this.show();
    }

    public show(): void {
        this.dirty = true;
        if (this.container) {
            this.container.visible = true;
        }
    }

    public getScore(): number {
        return this.score;
    }

    public hide(): void {
        if (this.container) {
            this.container.visible = false;
        }
    }

    public render(delta: number): void {
        if (this.label && this.dirty) {
            this.label.text = `Score: ${this.score}`;
            this.dirty = false;
        }
    }

    public getColliders(): CollidableShape[] {
        return [];
    }

    private setupElements(): void {
        this.label = new Text(`Score: ${this.score}`);
        this.label.style.fontFamily = this.font.family;
        this.label.style.fontSize = 30;
        this.label.style.fill = this.font.color;
        this.label.anchor.set(0.5, 1);
        this.label.x = this.app.view.width / 2;
        this.label.y = 140;
        this.dirty = true;

        this.container?.addChild(this.label);
    }
}