import { Container, Rectangle, Sprite } from "pixi.js";
import { SettingsPrefix } from "../../utils/arguments";
import { CollidableShape } from "../collision";
import { Renderable } from "../renderable";
import { sfx } from "../sfx";

export class Music extends Renderable {
    private music?: HTMLAudioElement;
    private container?: Container;
    
    public init(): Promise<void> {
        return new Promise((resolve) => {
            this.app.loader.reset();
            this.app.loader.add("music", "./assets/audio.mp3");
            this.app.loader.add("sound-on", "./assets/sound-on.png");
            this.app.loader.add("sound-off", "./assets/sound-off.png");
            this.app.loader.load(() => {
                this.music = this.app.loader.resources["music"].data as HTMLAudioElement;
                this.music.volume = 0.1;
                this.music.loop = true;

                try {
                    const muted: string = localStorage.getItem(SettingsPrefix() + "muted") ?? "0";
                    this.music.muted = muted === "1";
                } catch (e) {
                    // Do nothing, no local storage available.
                }

                this.container = new Container();
                this.container.x = 200;
                this.container.y = 100;
                
                const muteButton: Sprite = this.createButton("sound-on", !this.music.muted);
                const unmuteButton: Sprite = this.createButton("sound-off", this.music.muted);
                muteButton.addListener("click", () => this.toggleMute());
                unmuteButton.addListener("click", () => this.toggleMute());

                this.container.addChild(muteButton, unmuteButton);
                this.app.stage.addChild(this.container);

                window.addEventListener("keyup", (e) => {
                    if (e.key === "m" || e.key === "M") {
                        this.toggleMute();
                    }
                })

                resolve();
            });
        });
    }

    public playMusic(): void {
        this.music?.play();
    }

    public toggleMute(): void {
        if (this.music) {
            this.music.muted = !this.music.muted;
            sfx.setMute(this.music.muted);

            this.container?.children.forEach(x => {
                x.visible = !x.visible;
                x.interactive = !x.interactive;
            });

            try {
                localStorage.setItem("muted", (this.music.muted) ? "1" : "0");
            } catch (e) {
                // Do nothing, no local storage available.
            }
        }
    }

    public render(delta: number): void {
        // Do nothing
    }

    public getColliders(): CollidableShape[] {
        return [];
    }

    private createButton(texture: string, active: boolean): Sprite {
        const button: Sprite = new Sprite(this.app.loader.resources[texture].texture);
        button.width = 40;
        button.height = 40;
        button.visible = active;
        button.interactive = active;
        button.buttonMode = true;

        return button;
    }
}