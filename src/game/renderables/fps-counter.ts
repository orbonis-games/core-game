import { Text } from "pixi.js";
import { CollidableShape } from "../collision";
import { Renderable } from "../renderable";

export class FPSCounter extends Renderable {
    private label?: Text;
    private averageDelta: number = 0;

    public init(): Promise<void> {
        this.label = new Text("0FPS");
        this.label.x = 10;
        this.label.y = 10;
        this.label.style.fill = 0xAAAAAA;
        this.app.stage.addChild(this.label);

        window.setInterval(() => {
            if (this.label) {
                const fps: number = Math.round(1 / this.averageDelta);
                this.label.text = `${fps}FPS`;
            }
        }, 1000);

        return new Promise((resolve) => resolve());
    }

    public render(delta: number): void {
        this.averageDelta = (this.averageDelta + delta) / 2;
    }

    public getColliders(): CollidableShape[] {
        return [];
    }
}
