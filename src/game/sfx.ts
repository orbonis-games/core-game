import { Application } from "pixi.js";
import { SettingsPrefix } from "../utils/arguments";

export interface SFXData {
    id: string;
    url: string;
}

export class SFX {
    private sounds: { [key: string]: HTMLAudioElement } = {};

    private muted: boolean = false;

    public load(app: Application, sounds: SFXData[]): Promise<void> {
        return new Promise((resolve) => {
            app.loader.reset();
            sounds.forEach((x) => {
                app.loader.add(x.id, x.url);
            });
            app.loader.load(() => {
                sounds.forEach((x) => {
                    this.sounds[x.id] = app.loader.resources[x.id].data;
                });

                try {
                    const muted: string = localStorage.getItem(SettingsPrefix() + "muted") ?? "0";
                    this.muted = muted === "1";
                } catch (e) {
                    // Do nothing, no local storage available.
                }

                resolve();
            });
        });
    }

    public play(id: string): Promise<void> {
        if (id in this.sounds) {
            this.sounds[id].volume = 0.25;
            this.sounds[id].muted = this.muted;
            return this.sounds[id].play();
        } else {
            return new Promise((resolve) => resolve());
        }
    }

    public setMute(mute: boolean): void {
        this.muted = mute;
    }
}

export const sfx: SFX = new SFX();