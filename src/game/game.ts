import { Application, Point } from "pixi.js";
import { Renderable } from "./renderable";

import { update as TweenUpdate } from "@tweenjs/tween.js";
import { sfx, SFXData } from "./sfx";
import { IsDebug } from "../utils/arguments";
import { FPSCounter } from "./renderables/fps-counter";
import { Collision } from "./collision";

export class Game {
    protected app?: Application;

    private collision: Collision;
    private renderables: Renderable[];

    private delta: number;
    private lastUpdateTime?: number;

    constructor() {
        this.delta = 0;
        this.renderables = [];
        this.collision = new Collision();
    }

    public async init(canvas: HTMLCanvasElement, sounds?: SFXData[]): Promise<void> {
        this.app = new Application({
            view: canvas,
            autoStart: false,
            width: 1000,
            height: 1000,
            transparent: true
        });

        this.app.render();
        canvas.style.display = "block";

        this.collision.init(this.app);

        await this.initRenderables();
        if (sounds) {
            await sfx.load(this.app, sounds);
        }

        requestAnimationFrame((time) => this.render(time));
    }

    protected async initRenderables(...renderables: Renderable[]): Promise<void> {
        this.renderables.push(...renderables);

        if (IsDebug() && this.app) {
            this.renderables.push(new FPSCounter("fps", this.app));
        }

        for (const renderable of this.renderables) {
            await renderable.init();
        }

        this.collision.add(...renderables);
    }

    protected prerender(delta: number): void {
        // Do nothing
    }

    protected postrender(delta: number): void {
        // Do nothing
    }

    protected getCollision(): Collision {
        return this.collision;
    }

    private render(time: number): void {
        this.delta = (time - (this.lastUpdateTime ?? 0)) / 1000;

        if (this.lastUpdateTime) {
            TweenUpdate(time);

            this.collision.update();
            this.prerender(this.delta);

            this.renderables.forEach((i) => i.render(this.delta));
            this.app?.render();

            this.postrender(this.delta);
            this.collision.clear();
        }

        this.lastUpdateTime = time;
        requestAnimationFrame((time) => this.render(time));
    }
}
