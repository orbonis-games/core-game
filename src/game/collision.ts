import { Application, Circle, Graphics, Point, Polygon, Rectangle } from "pixi.js";
import Intersects from "intersects";
import { Line, Ray } from "../utils/ray";

export type CollidableShape = Rectangle | Circle | Polygon;

export interface Collidable {
    id: string;
    colliding?: string;
    blockVision: boolean;
    getColliders: () => CollidableShape[];
    isActiveCollider: () => boolean;
}

export class Collision {
    private colliders: Collidable[] = [];
    private app?: Application;
    private collisionDebug?: Graphics;

    public init(app: Application) {
        this.app = app;
    }

    public add(...colliders: Collidable[]): void {
        for (const collider of colliders) {
            const index: number = this.colliders.indexOf(collider);
            if (index === -1) {
                this.colliders.push(collider);
            }
        }
    }

    public remove(...colliders: Collidable[]): void {
        for (const collider of colliders) {
            const index: number = this.colliders.indexOf(collider);
            if (index !== -1) {
                this.colliders.splice(index, 1);
            }
        }
    }

    public removeAll(predicate: (collidable: Collidable) => boolean): void {
        const colliders = this.colliders.filter((x) => predicate(x));
        this.remove(...colliders);
    }

    public update(): void {
        const active: Collidable[] = this.colliders.filter((x) => x.isActiveCollider());

        for (const target of this.colliders) {
            const targetColliders = target.getColliders();
            for (const a of targetColliders) {
                for (const source of active) {
                    if (source === target) {
                        continue;
                    }
                    if (source.colliding === undefined) {
                        let breakout: boolean = false;
                        const sourceColliders = source.getColliders();
                        for (const b of sourceColliders) {
                            if (this.checkCollision(a, b)) {
                                source.colliding = target.id;
                                target.colliding = source.id;
                                breakout = true;
                                break;
                            }
                        }
                        if (breakout) {
                            break;
                        }
                    }
                }
            }
        }
    }

    public clear(): void {
        for (const collider of this.colliders) {
            collider.colliding = undefined;
        }
    }

    public raycast(root: Point): Point[] {
        const final: Line[] = [];
        const blockers: CollidableShape[] = this.colliders
            .filter((x) => x.blockVision)
            .reduce<CollidableShape[]>((prev, curr) => [...prev, ...curr.getColliders()], []);
        const lines: Line[] = [];
        const targets: Point[] = [];

        if (this.app) {
            blockers.push(new Rectangle(0, 0, this.app.view.width, this.app.view.height));
        }
        
        for (let shape of blockers) {
            if (shape instanceof Rectangle) {
                lines.push(new Line(shape.left, shape.top, shape.right, shape.top));
                lines.push(new Line(shape.right, shape.top, shape.right, shape.bottom));
                lines.push(new Line(shape.left, shape.top, shape.left, shape.bottom));
                lines.push(new Line(shape.left, shape.bottom, shape.right, shape.bottom));

                targets.push(new Point(shape.left, shape.top));
                targets.push(new Point(shape.right, shape.top));
                targets.push(new Point(shape.left, shape.bottom));
                targets.push(new Point(shape.right, shape.bottom));
            } else if (shape instanceof Polygon) {
                for (let i = 0; i < shape.points.length - 3; i += 4) {
                    lines.push(new Line(
                        shape.points[i],
                        shape.points[i + 1],
                        shape.points[i + 2],
                        shape.points[i + 3]
                    ));
                    targets.push(new Point(shape.points[i], shape.points[i + 1]));
                    targets.push(new Point(shape.points[i + 2], shape.points[i + 3]));
                }
            }
        }

        if (!this.collisionDebug) {
            this.collisionDebug = new Graphics();
            this.app?.stage.addChild(this.collisionDebug);
        }

        this.collisionDebug.clear();
        this.collisionDebug.lineStyle(2, 0xFF0000);
        for (let target of targets) {
            this.collisionDebug.beginFill(0xFF0000, 0.5);
            this.collisionDebug.drawCircle(target.x, target.y, 10);
            this.collisionDebug.endFill();
        }

        for (let i = 0; i < targets.length; i++) {
            const ray: Ray = new Ray();
            ray.position = root.clone();
            ray.look(targets[i].x, targets[i].y);

            const hits: Line[] = [];

            for (const line of lines) {
                const cast = ray.cast(line);
                if (cast) {
                    hits.push(new Line(root.x, root.y, cast.x, cast.y));

                    if (cast.x === targets[i].x && cast.y === targets[i].y) {
                        const tLine: Line = new Line(root.x, root.y, targets[i].x, targets[i].y);
                        targets.push(tLine.clone().rotate(tLine.rotation() + 0.00001).end());
                        targets.push(tLine.clone().rotate(tLine.rotation() - 0.00001).end());
                    }
                }
            }

            if (hits.length > 0) {
                let line: Line = hits[0];
                for (let i = 1; i < hits.length; i++) {
                    if (hits[i].length() < line.length()) {
                        line = hits[i];
                    }
                }
    
                final.push(line.clone());
            }
        }

        final.sort((a, b) => a.rotation() - b.rotation());

        return final.map((x) => new Point(x.x2, x.y2));
    }

    private checkCollision(a: CollidableShape, b: CollidableShape): boolean {
        if (a instanceof Circle && b instanceof Circle) {
            return Intersects.circleCircle(a.x, a.y, a.radius, b.x, b.y, b.radius);
        }
        if (a instanceof Rectangle && b instanceof Circle) {
            return Intersects.boxCircle(a.x, a.y, a.width, a.height, b.x, b.y, b.radius);
        }
        if (a instanceof Circle && b instanceof Rectangle) {
            return Intersects.boxCircle(b.x, b.y, b.width, b.height, a.x, a.y, a.radius);
        }

        if (a instanceof Rectangle && b instanceof Rectangle) {
            return Intersects.boxBox(a.x, a.y, a.width, a.height, b.x, b.y, b.width, b.height);
        }

        if (a instanceof Circle && b instanceof Polygon) {
            return Intersects.circlePolygon(a.x, a.y, a.radius, b.points);
        }
        if (a instanceof Polygon && b instanceof Circle) {
            return Intersects.circlePolygon(b.x, b.y, b.radius, a.points);
        }

        if (a instanceof Rectangle && b instanceof Polygon) {
            return Intersects.boxPolygon(a.x, a.y, a.width, a.height, b.points);
        }
        if (a instanceof Polygon && b instanceof Rectangle) {
            return Intersects.boxPolygon(b.x, b.y, b.width, b.height, a.points);
        }

        if (a instanceof Polygon && b instanceof Polygon) {
            return Intersects.polygonPolygon(a.points, a.points);
        }

        return false;
    }
}
