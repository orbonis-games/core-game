import { Application } from "pixi.js";
import { Collidable, CollidableShape } from "./collision";

export abstract class Renderable implements Collidable {
    public colliding?: string;
    public blockVision: boolean = false;

    constructor(public readonly id: string, protected app: Application) { }

    public abstract init(): Promise<void | void[]>;
    public abstract render(delta: number): void;
    public abstract getColliders(): CollidableShape[];

    public isActiveCollider(): boolean {
        return false;
    }
}