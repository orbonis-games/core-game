import React from "react";
import { GameCanvas } from "./components/game-canvas";
import { Game } from "./game/game";
import { HideRepo, TransparentBackground } from "./utils/arguments";

interface IProperties {
    bitbucket: string;
    audioCredit: string;
    game: typeof Game;
}
interface IState {}

export class Application extends React.Component<IProperties, IState> {
    private game: Game;

    constructor(props: IProperties) {
        super(props);

        this.state = {};

        this.game = new this.props.game();

        if (TransparentBackground()) {
            document.body.style.backgroundColor = "transparent";
        }
    }

    public render(): JSX.Element[] {
        return [
            <GameCanvas key="game-canvas" onMount={(canvas) => this.game.init(canvas)} />,
            <div key="repo-link" className="repo-link" style={{ display: ((this.props.bitbucket.length > 0 && !HideRepo()) ? "block" : "none")}}>
                <a target="_blank" href={this.props.bitbucket}>
                    Bitbucket Repository
                </a>
            </div>,
            <div key="audio-link" className="audio-link">
                <a target="_blank" href="https://creativecommons.org/licenses/by/4.0/">
                    { this.props.audioCredit }
                </a>
            </div>
        ];
    }
}