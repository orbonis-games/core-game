import { parse } from "search-params";

const params = parse(location.search);

export const IsDebug = (): boolean => {
    return "debug" in params && params["debug"] !== false;
};

export const HideRepo = (): boolean => {
    return "hideRepo" in params && params["hideRepo"] !== false;
}

export const TransparentBackground = (): boolean => {
    return "transparent" in params && params["transparent"] !== false;
}

export const SettingsPrefix = (): string => {
    return ("prefix" in params && params["prefix"]) ? params["prefix"] as string : "";
}